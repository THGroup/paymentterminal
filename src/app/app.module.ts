import { CurrencyMaskDirective } from './shared/currency.directive';
import { PhoneMaskDirective } from './shared/phone-mask.directive';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainComponent } from './main/main.component';
import { ReFillBalanceComponent } from './reFillBalance/re-fill-balance.component';
import { OperatorService } from './shared/services/operator.service';
import { BalanceService } from './shared/services/balance.service';
import { RefactoringComponent } from './refactoring/refactoring.component';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    ReFillBalanceComponent,
    PhoneMaskDirective,
    CurrencyMaskDirective,
    RefactoringComponent,
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [
    AppService,
    BalanceService,
    OperatorService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
