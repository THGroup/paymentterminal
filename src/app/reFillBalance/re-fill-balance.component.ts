import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from '../app.service';
import { IOperator } from '../shared/interface/operator.interface';
import { BalanceService } from '../shared/services/balance.service';
import { OperatorService } from '../shared/services/operator.service';

@Component({
  selector: 'app-re-fill-balance',
  templateUrl: './re-fill-balance.component.html',
  styleUrls: ['./re-fill-balance.component.scss']
})
export class ReFillBalanceComponent implements OnInit {

  public saving: boolean = false;
  public operatorId!: number;
  public operator!: IOperator;
  public reFillForm!: FormGroup;
 
  constructor(
    private operatorService: OperatorService,
    private balanceService: BalanceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private appService: AppService,
  ) {}

  public ngOnInit(): void {
    this.operatorId = this.activatedRoute.snapshot.params.id;
    this.getOperator();
    this.initializeForm();
  }

  public getOperator(): void {
    this.operatorService.getOperatorById(this.operatorId).subscribe(data => {
      this.operator = data;
    });
  }

  public initializeForm(): void {
    this.reFillForm = new FormGroup({
      operatorId: new FormControl(this.operatorId),
      phone: new FormControl('', [Validators.required]),
      amount: new FormControl('', [Validators.required, Validators.min(1), Validators.max(1000)])
    });
  }

  public submit(): void {
    this.saving = true;
    /* stop here if reFillForm is invalid */
    if (this.reFillForm.invalid) {
        return;
    }

    this.balanceService.saveBalance(this.reFillForm.value).subscribe(
      () => {
        //generate random number.
        if (Math.floor(Math.random() * Math.floor(2)) == 0) {  
          this.appService.alertMessage.next({
            type: 'success',
            message: 'Balance Successfully saved.'
          });
          this.goToMainPage();
        } else {
          this.appService.alertMessage.next({
            type: 'danger',
            message: 'Error. Try again.'
          });
        }
        this.saving = false;		
      },
      error => {
        console.log(error);
        this.appService.alertMessage.next({
          type: 'danger',
          message: 'Error. Try again.'
        });
        this.saving = false;
      }
    );
  }

  public goToMainPage(): void {
    this.router.navigate(['/']);
  }
}
