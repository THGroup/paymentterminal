import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'refactoring',
  templateUrl: './refactoring.component.html',
  styleUrls: ['./refactoring.component.scss']
})
export class RefactoringComponent implements OnInit {

  public characterString = 'testing String';
  public characterA = 's';
  public characterB = 'g';
  public characterPosition = -1;

  public ngOnInit(): void {

    this.characterPosition = this.getCharacterPositionFromString(this.characterString, this.characterA, this.characterB);
  }

  /* Find a two characters position backward from a string, And return the biggest number   */
  public getCharacterPositionFromString(characterString: string, CharacterA: string, CharacterB: string): number {

    /* check empty string */
    if (characterString.match(/^$/)) {
      return -1;
    }
    /* check CharacterA and CharacterB length */
    const CharacterAIndex = (CharacterA && CharacterA.length === 1) ? characterString.lastIndexOf(CharacterA) : -1;
    const CharacterBIndex = (CharacterB && CharacterB.length === 1) ? characterString.lastIndexOf(CharacterB) : -1;

    return Math.max(CharacterAIndex, CharacterBIndex);
  }
}