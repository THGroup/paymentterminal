import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { RefactoringComponent } from './refactoring/refactoring.component';
import { ReFillBalanceComponent } from './reFillBalance/re-fill-balance.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'refill/:id', component: ReFillBalanceComponent },
  { path: 'refactoring', component: RefactoringComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
