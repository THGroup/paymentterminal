import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
export interface IAlert {
  type: string;
  message: string;
}

@Injectable()
export class AppService {
  
  public alertMessage = new Subject<IAlert>();
  constructor() {}
}
