import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IOperator } from '../shared/interface/operator.interface';
import { OperatorService } from '../shared/services/operator.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public operators: IOperator[] = [];
  constructor(
    private operatorService: OperatorService,
    private router: Router,
  ) {}

  public ngOnInit(): void {
    this.getOperators();
  }

  public getOperators(): void {
    this.operatorService.getOperators().subscribe((data: IOperator[]) => {
      this.operators = data;
    });
  }

  public selectOperator(operator: IOperator): void {
    this.router.navigate([`refill`, operator.id]);
  }
}
