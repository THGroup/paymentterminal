import { Component, ViewChild } from '@angular/core';
import { NgbAlert } from '@ng-bootstrap/ng-bootstrap';
import {debounceTime} from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AppService, IAlert } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  
  public alert: IAlert | null = null;
  @ViewChild('alertMessage', { static: false }) closingAlert!: NgbAlert;
  
  constructor(private appService: AppService) {
    this.appService.alertMessage.subscribe(alertValue => this.alert = alertValue);
    this.appService.alertMessage.pipe(debounceTime(environment.alertVisibleTime)).subscribe(() => {
      if (this.closingAlert) {
        this.closingAlert.close();
      }
    });
  }
}
