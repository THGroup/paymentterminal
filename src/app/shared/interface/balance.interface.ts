export interface IBalance {
    id: number;
    operatorId: string;
    phone: string;
    amount: string;
}