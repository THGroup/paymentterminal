import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IBalance } from '../interface/balance.interface';

@Injectable()
export class BalanceService {
  
  public apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) { }

  public saveBalance(balance: IBalance): Observable<IBalance> {
      return this.http.post<IBalance>(`${this.apiUrl}balance`, balance);
  }
}
