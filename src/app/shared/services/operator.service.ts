import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IOperator } from '../interface/operator.interface';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable()
export class OperatorService {
    public apiUrl = environment.apiUrl;
    constructor(private http: HttpClient) { }

    public getOperators(): Observable<IOperator[]> {
        return this.http.get<IOperator[]>(`${this.apiUrl}operator`);
    }
    public getOperatorById(id: number): Observable<IOperator> {
        return this.http.get<IOperator>(`${this.apiUrl}operator/${id}`);
    }
}
