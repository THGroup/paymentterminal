import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';
import { clone } from 'lodash';

@Directive({
  selector: '[currencyMask]',
})
export class CurrencyMaskDirective { 

  constructor(private ngControl: NgControl) { }

  @HostListener('ngModelChange', ['$event'])
  onModelChange(event: any) {
    this.onInputChange(event, false);
  }

  @HostListener('keydown.backspace', ['$event'])
  keydownBackspace(event: any) {
    this.onInputChange(event.target.value, true);
  }
  

  onInputChange(event: string, backspace: boolean) {
    let newVal = event.replace(/\D/g, '');
    if (newVal.length === 0) {
      newVal = '';
    } else if (newVal.length <= 3) {
      newVal = newVal.replace(/^(\d{0,3})/, '$1');
    } else {
      newVal = newVal.substring(0, 4);
      newVal = newVal.replace(/^(\d{0,1})(\d{1,3})/, '$1,$2');
    }
    this.ngControl.valueAccessor?.writeValue("₽"+ newVal);
    let value = clone(newVal);
    value = value.replace(',', '');
    if (+value > 1000) {
        this.ngControl.control?.setErrors({"max": true});
    }
    if (+value < 1) {
        this.ngControl.control?.setErrors({"min": true});
    }
  }
}
