export const environment = {
  production: true,
  apiUrl: 'http://localhost:4200/api/',
  alertVisibleTime: 5000,
};
